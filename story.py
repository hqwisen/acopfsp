"""
Run six algorithms for all instances and write data in data/ folder.
"""
import copy
import glob
import logging
import time
from multiprocessing import Pool, Value
from pathlib import Path

import numpy as np

# Configure logging
import acopfsp

FORMAT = '%(asctime)s - %(levelname)s - %(module)s - %(message)s'
DEFAULT_LEVEL = logging.NOTSET

logger = logging.getLogger(__name__)
formatter = logging.Formatter(fmt=FORMAT)
handlers = [logging.StreamHandler()]
for handler in handlers:
    handler.setFormatter(formatter)
    logger.addHandler(handler)
logger.setLevel(DEFAULT_LEVEL)

counter = None
nexecutions = None
start = None

nruns = 10

# Best configurations provided by irace
runs = [{
    'acs': ['--acs', '--local-search', 'disable', '--ants', '41',
            '--alpha', '2.56', '--beta', '7.78', '--rho', '0.32', '--xi', '0.46', '--q0', '0.07'],
    'acs_ls_best': ['--acs', '--local-search', 'best', '--ants', '5',
                    '--alpha', '2.83', '--beta', '3.28', '--rho', '0.61', '--xi', '0.38', '--q0',
                    '0.04'],
    'acs_ls_all': ['--acs', '--local-search', 'all', '--ants', '93',
                   '--alpha', '3.69', '--beta', '5.08', '--rho', '0.05', '--xi', '0.77', '--q0',
                   '0.24'],
    'mmas': ['--mmas', '--local-search', 'disable', '--ants', '28', '--alpha', '3.88',
             '--beta', '6.69', '--rho', '0.53'],
    'mmas_ls_best': ['--mmas', '--local-search', 'best', '--ants', '34',
                     '--alpha', '4.41', '--beta', '9.48', '--rho', '0.41'],
    'mmas_ls_all': ['--mmas', '--local-search', 'all', '--ants', '87',
                    '--alpha', '0.34', '--beta', '6.76', '--rho', '0.7'],
    'id': i,
    'seed': np.random.randint(100000)
} for i in range(nruns)]


def execacopfsp(values):
    global counter, nexecutions, start
    rid, rseed, instance, args = values
    logger.info(f"Executing run #{rid}, seed={rseed}, "
                f"instance={instance}, cmdline={args}")
    acopfsp.cmdline(args)
    with counter.get_lock():
        counter.value += 1
    print(f"Executed: {counter.value}/{nexecutions.value}, elapsed {time.time() - start.value}s")


def init_pool(args):
    global counter, nexecutions, start
    counter, nexecutions, start = args


def main():
    global counter, nexecutions, start
    instances = glob.glob('instances/*.txt')
    executions = []
    # pool = Pool(len(runs) * len(instances))
    for run in runs:
        for instance in instances:
            instance = Path(instance)
            algos = ['acs', 'mmas', 'acs_ls_best', 'acs_ls_all',
                     'mmas_ls_best', 'mmas_ls_all']
            # algos = ['acs', 'mmas', 'mmas_ls_best']
            for algoid in algos:
                args = copy.copy(run[algoid])
                args.extend(["--seed", str(run['seed']), "-i", f"{instance}",
                             '--collect-data', '--data-folder',
                             f"data/run__{instance.stem}__{algoid}_{run['id']}",
                             ])
                # Because we use multithreading, we set the stopping condition to
                # 2500 steps. Which corresponds to a budget of 30 seconds
                # wall-clock execution time in standalone mode
                args.extend(['--steps', '2500', '-mt', '110', '-it', '500'])
                executions.append((run['id'], run['seed'], instance, args))

    nexecutions = len(executions)
    print(f"Executing {nexecutions} acopfsp")
    # Running on a machine with 32 CPUs, update accordingly
    counter = Value('i', 0)
    nexecutions = Value('i', len(executions))
    start = Value('f', time.time())
    print(counter, nexecutions, start)
    pool = Pool(32, initializer=init_pool, initargs=((counter, nexecutions, start),))
    pool.map(execacopfsp, executions)


if __name__ == "__main__":
    main()
