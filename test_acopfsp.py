from acopfsp import *


def _test_instance(instance):
    pfsp_wt = PFSPWT(instance)
    assert pfsp_wt.njobs == 50
    assert pfsp_wt.nmachines == 20
    # Test with ACS algo
    acs = ACOACS(pfsp_wt)
    print(pfsp_wt.weights)



def test_DD_ta051():
    _test_instance('instances/DD_Ta051.txt')
