#!/usr/bin/env python3

import argparse
import glob
import json
import logging
import sys
import time
from functools import partial
from pathlib import Path

import numpy as np

# Configure logging

FORMAT = '%(asctime)s - %(levelname)s - %(module)s - %(message)s'
DEFAULT_LEVEL = logging.NOTSET

logger = logging.getLogger(__name__)
formatter = logging.Formatter(fmt=FORMAT)
handlers = [logging.StreamHandler()]
# handlers.append(logging.FileHandler(filepath))
for handler in handlers:
    handler.setFormatter(formatter)
    logger.addHandler(handler)
logger.setLevel(DEFAULT_LEVEL)

RELDUE = 'Reldue'


class PFSPWT(object):
    """
    Class to parse the instance files and retrieve machines and jobs data.
    """

    def __init__(self, instance):
        self.instance = Path(instance)
        # self.ptimes[i][j] = Processing time of job i on machine j
        self.ptimes = None
        self.duedates = None
        self.njobs, self.nmachines = None, None
        self._parse()
        # Cache variable, to store some computation
        self._cache = {
            'ct_matrix': {},
        }

    @property
    def name(self):
        return self.instance.name

    def _parse_processing_times(self, f):
        njob, line = 0, self._readline(f)
        while line != RELDUE:
            logger.debug(f"njob for line: {njob}")
            values = list(map(int, line.split()))
            for nmachine, ptime in zip(values[0::2], values[1::2]):
                if ptime < 0:
                    raise ValueError(f"Cannot have negative processing time "
                                     f"({ptime}) for job {njob}")
                self.ptimes[njob][nmachine] = ptime
            njob, line = njob + 1, self._readline(f)
        if njob != self.njobs:
            raise RuntimeError(f"Failed parsing instance '{self.instance}': "
                               f"found {njob} lines for jobs processing time, "
                               f"when {self.njobs} where expected.")
        logger.debug(f"Processing time matrix: \n {self.ptimes}")
        logger.info("Processing times parsed")

    def _parse_due_dates(self, f):
        njob, line = 0, self._readline(f)
        while line:
            _, duedate, _, priority_weight = map(int, line.split())
            self.duedates[njob], self.weights[njob] = duedate, priority_weight
            njob, line = njob + 1, self._readline(f)
        if njob != self.njobs:
            raise RuntimeError(f"Failed parsing instance '{self.instance}': "
                               f"found {njob} lines for jobs due dates, "
                               f"when {self.njobs} were expected.")
        logger.debug(f"Due dates: \n {self.duedates}")
        logger.debug(f"Weights: \n {self.weights}")
        logger.info("Due dates parsed")

    def _parse(self):
        logger.info(f"PFSP-WT: Parsing instance {self.instance}")
        with open(self.instance) as f:
            self.njobs, self.nmachines = map(int, self._readline(f).split())
            logger.info(f"# of jobs: {self.njobs}")
            logger.info(f"# of machines: {self.nmachines}")
            if self.njobs == 0 or self.nmachines == 0:
                logger.warning("Number of jobs or number of machines is null!")
                return
            self.ptimes = np.full((self.njobs, self.nmachines), -1)
            self.duedates = np.full(self.njobs, -1)
            self.weights = np.full(self.njobs, -1)
            logger.info(f"Matrix size (nj x nm): {len(self.ptimes)}x{len(self.ptimes[0])}")
            self._parse_processing_times(f)
            self._parse_due_dates(f)

    @staticmethod
    def _readline(f):
        line = f.readline().strip()
        logger.debug(f"reading line {line}")
        return line

    def completion_time(self, schedule_index, nmachine, schedule):
        hashsch = tuple(schedule)
        if hashsch not in self._cache['ct_matrix']:
            matrix = self.build_completion_matrix(schedule)
            self._cache['ct_matrix'][hashsch] = matrix
        return self._cache['ct_matrix'][hashsch][nmachine][schedule_index]

    def build_completion_matrix(self, schedule):
        """
        Build a completion time matrix of a schedule.
        A completion time matrix correspond to a matrix where the each line represent
        the completion times of the jobs for each machine.
        """
        matrix = []
        m0ptimes = [self.ptimes[njob][0] for njob in schedule]
        m0 = np.add.accumulate(m0ptimes)
        matrix.append(m0)
        for nmachine in range(1, self.nmachines):
            m = [matrix[-1][0] + self.ptimes[schedule[0]][nmachine]]
            for si in range(1, self.njobs):
                jobbefore = m[si - 1]
                machinebefore = matrix[nmachine - 1][si]
                m.append(max(jobbefore, machinebefore) + self.ptimes[schedule[si]][nmachine])
            matrix.append(np.array(m))
        return matrix


class Ant(object):
    def __init__(self, aco, nant):
        self.aco = aco
        self.nant = nant
        self._solution = None
        self.twt = None
        self.reset()

    def build_solution(self):
        """
        Build a schedule solution using either exploration or best method.
        MMAS only uses exploration.
        """
        self.reset()
        for pos in self.aco.jobs:
            select = self.exploration
            if self.aco.is_acs:
                select = np.random.choice([self.best, self.exploration],
                                          p=[self.aco.usebest, 1 - self.aco.usebest])
            njob = select(pos)
            if self.aco.is_acs:
                self.aco.local_update_trails(njob, pos)
            self._solution.append(njob)
        self.twt = self.aco.total_wt(self.solution)
        logger.info(f"Ant #{self.nant} builded solution: {self.twt}")

    def exploration(self, pos):
        logger.debug(f"Ant #{self.nant} EXPLORATION method for position {pos}")
        unscheduled = self.unscheduled()
        sum_unscheduled_tau_eta = np.sum([self.aco.tau_eta(njob, pos) for njob in unscheduled])
        logger.debug(f"Sum of unscheduled tau_eta: {sum_unscheduled_tau_eta}")
        p = np.empty(len(unscheduled))
        for index, njob in enumerate(unscheduled):
            p[index] = self.aco.tau_eta(njob, pos) / sum_unscheduled_tau_eta
        return np.random.choice(unscheduled, p=p)

    def best(self, pos):
        logger.debug(f"Ant #{self.nant} BEST method for position {pos}")
        unscheduled = self.unscheduled()
        return unscheduled[np.argmax(map(lambda njob: self.aco.tau_eta(njob, pos), unscheduled))]

    def localsearch(self):
        """
        Performs an exhaustive local search, that is check all permutation (i, j) of jobs.
        """
        for i in range(self.aco.njobs):
            for j in range(self.aco.njobs):
                if i != j:
                    localsolution = np.insert(np.delete(self._solution, i), j, self._solution[i])
                    local_twt = self.aco.total_wt(localsolution)
                    if self.aco.is_acs:
                        self.aco.local_update_trails(localsolution[i], i)
                    # logger.debug(f"Local search Ant #{self.nant} for ({i}, {j})")
                    if local_twt < self.twt:
                        logger.info(
                            f"Step #{self.aco.step}, local search found "
                            f"better sol. in local search ant #{self.nant} "
                            f"for ({i}, {j}), TWT = {local_twt}")
                        self._solution = localsolution
                        self.twt = local_twt
                    yield (localsolution, local_twt)

    def reset(self):
        self._solution = []

    @property
    def solution(self):
        return np.array(self._solution)

    def unscheduled(self):
        unscheduled = np.delete(self.aco.jobs, self.solution)
        logger.debug(f"Ant #{self.nant} Unscheduled jobs: {unscheduled}")
        return unscheduled


class ACO(object):
    def __init__(self, name, is_acs, is_mmas, pfsp_wt,
                 seed, iterations, steps, max_time, localsearch,
                 nants, alpha, beta, rho, xi, usebest, heuristic,
                 reinit_trails_after, collect_data, data_folder):
        self._name = name
        self.is_acs = is_acs
        self.is_mmas = is_mmas
        # Config parameters
        self.pfsp_wt = pfsp_wt
        if seed < 0:
            logger.info("Negative seed: setting seed to use current system time.")
            seed = None
        self.seed = seed
        self.iterations = iterations
        self.steps = steps
        self.max_time = max_time
        self.all_localsearch = localsearch == 'all'
        self.best_localsearch = localsearch == 'best'
        self.nants = nants
        self.alpha = alpha
        self.beta = beta
        self.rho = rho
        self.xi = xi
        self.usebest = usebest
        self.heuristic_func = heuristic
        self.reinit_trails_after = reinit_trails_after
        self.collect_data = collect_data
        self.data_folder = data_folder
        self.jobs = np.arange(self.pfsp_wt.njobs)  # Index of jobs
        # Algorithm parameters
        self.t = 1
        self.step = 1
        self.ants = [Ant(self, nant) for nant in range(self.nants)]
        # ph_trails[i][j] = desire of putting job i in jth position
        # Params set in initialize() at runtime
        # previous = last iteration, past = last step for reinit.
        self.solution, self.previous_solution, self.past_solution = None, None, None
        self.best_twt = None
        self.best_ant = None
        self.heuristic = None
        self.ph_trails = None
        self.start_time = None
        # Additional setup
        logger.info(f"Setting random seed to {self.seed}")
        np.random.seed(self.seed)

        if self.iterations <= 0:
            logger.warning(f"# of iterations is negative or null, no iterations will run")
        self.initialize()
        self._log_vars()
        self.jsondata = {
            'config': {
                'instance': self.pfsp_wt.name
            },
            'iter': {
                'total_wt': [],
                'sol_changed': []
            },
            'step': {
                'total_wt': []
            }
        }
        self.npdata = {
            'iter': {
                'solutions': [],
                'ph_trails': [],
            },
            'step': {
                'solutions': [],
            }
        }

    def _log_vars(self):
        logger.info(f"Init ACO {self.name} with the following vars:")
        _vars = self._vars()
        for var in _vars:
            logger.info(f"{var} = {_vars[var]}")

    def _vars(self):
        exclude_vars = ['jobs', 'pfsp_wt', 'ants', 'solution', 'heuristic_desir',
                        'ph_trails', '_cache']
        self_vars = vars(self)
        return {k: self_vars[k] for k in self_vars if k not in exclude_vars}

    def run(self):
        """
        Run the ACO algorithm.
        :return: (solution, total_wt, elapsed_time)
        """
        logger.info(f"Running ACO {self.name} for instance {self.pfsp_wt.name}")
        self._collect_iteration_data()
        self.start_time = time.clock()
        while self.t <= self.iterations and self.elapsed_time <= self.max_time \
                and self.step <= self.steps:
            logger.info(f"Iteration #{self.t}, {self.step} steps, "
                        f"elapsed {self.elapsed_time}s, "
                        f"best_twt = {self.best_twt}")
            self.previous_solution = self.solution
            for ant in self.ants:
                if self.elapsed_time > self.max_time or self.step > self.steps:
                    break
                ant.build_solution()
                best_twt = self.best_twt
                if ant.twt < best_twt:
                    logger.info(f"Ant #{ant.nant} has a better solution: {ant.twt}")
                    self.solution = ant.solution
                    self.best_twt = ant.twt
                    self.best_ant = ant
                self.step += 1
                self._collect_step_data()
                if self.all_localsearch:
                    self.localsearch(ant)

            if self.best_localsearch and self.best_ant is not None:
                logger.info(f"Running local search only for best ant #{self.best_ant.nant}")
                self.localsearch(self.best_ant)
            # Global Update uses the self.solution permutation meaning that
            # it computes a best-so-far update. In MMAS the iteration-best can also
            # be used, but it is not the case here.
            self.global_update_trails()
            if self.is_mmas:
                self.update_trail_limits()
            if self.reinit_trails_after > 0 and self.t % self.reinit_trails_after == 0:
                # If solution did not change for a while (reinit_trails_after param),
                # then reinit. trails.
                diff = self.total_wt(self.past_solution) - self.total_wt(self.solution)
                logger.info(f"Checking for stagnation at {self.t}, "
                            f"total_wt(past) - total_wt(now) = {diff}")
                if diff >= 0:
                    logger.warning(f"Stagnation detected at {self.t}, reinit. trails")
                    self.initialize_trails()
                self.past_solution = self.solution
            self.t += 1
            self._collect_iteration_data()
        logger.info(f"Stopped after {self.t} iterations, {self.step} steps, "
                    f"and {self.elapsed_time}s")
        logger.info(f"Best solution found: {self.solution}")
        print(f"Stats: stopped after {self.t} iterations, {self.step} steps, "
              f"and {self.elapsed_time}s")
        print(f"Solution: {self.solution}")
        print(f"Total WT: {self.total_wt(self.solution)}")
        self._write_data_to_file()
        return self.solution, self.total_wt(self.solution), self.elapsed_time

    def localsearch(self, ant):
        logger.info(f"Starting local search for ant #{ant.nant}")
        local_start = time.time()
        for (solution, twt) in ant.localsearch():
            if self.elapsed_time > self.max_time or self.step > self.steps:
                break
            if twt < self.best_twt:
                logger.info(f"Ant #{ant.nant} at step {self.step} "
                            f"has a better (local search) solution: {twt}")
                self.solution = solution
                self.best_twt = twt
            self.step += 1
            self._collect_step_data()
        logger.info(f"Local search took {round(time.time() - local_start, 2)}")

    def initialize(self):
        """
        Initial parameters values.

        The order is important, as the heuristic must first set the initial solution before
        being able to initial the pheromone trails (tau0, max-min, etc.).
        """
        self.initialize_heuristic()
        self.initialize_params()
        self.initialize_trails()

    def initialize_heuristic(self):
        """
        Initialize heuristic information using the method define in the config.
        """
        heuristic_func = getattr(self, f"{self.heuristic_func}_heuristic")
        heuristic_func()

    def initialize_params(self):
        """
        Implemented in child classes.
        """
        raise NotImplementedError

    def initialize_trails(self):
        self.ph_trails = np.full((self.pfsp_wt.njobs, self.pfsp_wt.njobs), self.initial_trail())

    def random_heuristic(self):
        """
        Random heuristic.

         - Random initial schedule
         - Set eta[i][j] = 1 / total_wt
        """
        logger.info("Using Random Heuristic for initial solution")
        self.solution = np.random.choice(self.jobs, size=self.pfsp_wt.njobs, replace=False)
        self.previous_solution = self.solution
        self.past_solution = self.solution
        self.best_twt = self.total_wt(self.solution)
        logger.info(f"Initial solution: \n{self.solution}")
        self.heuristic = 1 / np.full(self.pfsp_wt.njobs, self.best_twt)

    def edd_heuristic(self):
        """
        Earliest Due Date (EDD) heuristic.

         - Puts the jobs in non-decreasing order of the due dates d[njob]
         - Set eta[i][j] = 1 / duedates[njob]
        """
        logger.info("Using EDD Heuristic for initial solution")
        self.solution = np.argsort(self.pfsp_wt.duedates)
        self.previous_solution = self.solution
        self.past_solution = self.solution
        self.best_twt = self.total_wt(self.solution)
        logger.info(f"Initial solution: \n{self.solution}")
        self.heuristic = 1 / self.pfsp_wt.duedates

    def total_wt(self, schedule):
        """
        Compute the total weighted tardiness of a schedule.:
        """
        # Use jobs as list of indices, to avoid recreating an array with indices.
        # Semantically, this array will be interpreted as indices of the schedule provided,
        # to compute the completion times.
        tardiness = np.vectorize(partial(self.tardiness_of, schedule))(self.jobs)
        twt = np.dot(self.pfsp_wt.weights, tardiness)
        logger.debug(f"Total WT for schedule {schedule}: {twt}")
        return twt

    def tardiness_of(self, schedule, schedule_index):
        njob = schedule[schedule_index]
        tardiness = max(self.completion_time(schedule_index, self.last_machine, schedule) -
                        self.pfsp_wt.duedates[njob], 0)
        logger.debug(f"Tardiness of position {schedule_index} in schedule {schedule}: {tardiness}")
        return tardiness

    def completion_time(self, *args):
        return self.pfsp_wt.completion_time(*args)

    def tau_eta(self, njob, pos):
        """
        Compute tau[njob][pos] * eta[njob][pos]^beta
        """
        trail = self.ph_trails[njob][pos]
        eta = self.heuristic[njob]
        return np.multiply(np.power(trail, self.alpha), np.power(eta, self.beta))

    @property
    def elapsed_time(self):
        return round(time.clock() - self.start_time, 2)

    @property
    def last_machine(self):
        return self.pfsp_wt.nmachines - 1

    @property
    def name(self):
        return self._name.upper()

    @property
    def njobs(self):
        return self.pfsp_wt.njobs

    def _collect_iteration_data(self):
        if self.collect_data:
            jsondata = self.jsondata['iter']
            npdata = self.npdata['iter']
            jsondata['total_wt'].append(int(self.total_wt(self.solution)))
            jsondata['sol_changed'].append(
                self.solution.tolist() != self.previous_solution.tolist())
            npdata['solutions'].append(np.copy(self.solution))
            npdata['ph_trails'].append(np.copy(self.ph_trails))

    def _collect_step_data(self):
        if self.collect_data:
            jsondata = self.jsondata['step']
            npdata = self.npdata['step']
            jsondata['total_wt'].append(int(self.total_wt(self.solution)))
            npdata['solutions'].append(np.copy(self.solution))

    def _write_data_to_file(self):
        if self.collect_data:
            self.jsondata['steps'] = self.step
            self.jsondata['iterations'] = self.t
            self.jsondata['best_twt'] = int(self.best_twt)
            self.jsondata['elapsed_time'] = self.elapsed_time
            folder = Path(self.data_folder)
            folder.mkdir(parents=True, exist_ok=True)

            for d in self.npdata:
                np.save(folder / Path(d), self.npdata[d])
            np.save(folder / Path('bestschedule'), self.solution)

            filepath = folder / Path('aco.json')
            with open(filepath, 'w') as f:
                json.dump(self.jsondata, f, indent=2)

    def initial_trail(self):
        """
        Implemented in child classes.
        """
        raise NotImplementedError

    def update_trail_limits(self):
        """
        Implemented in child classes.
        """
        raise NotImplementedError

    def local_update_trails(self, njob, pos):
        """
        Implemented in child classes.
        """
        raise NotImplementedError

    def global_update_trails(self):
        """
        Implemented in child classes.
        """
        raise NotImplementedError


class ACOACS(ACO):
    def __init__(self, **kwargs):
        self.tau0 = None
        super(ACOACS, self).__init__('acs', **kwargs)

    def initialize_params(self):
        """
        tau0 = 1 / (njobs * total_wt)

        where total_wt correspond to the total weighted tardiness of the solution generated
        but the metaheuristic used.
        """
        self.tau0 = np.divide(1, np.multiply(self.pfsp_wt.njobs,
                                             self.total_wt(self.solution)))

    def initial_trail(self):
        if self.t > 1:
            logger.warning("Accessing initial trail after initialization! "
                           "(Ignore if reinit enabled)")
        return self.tau0

    def update_trail_limits(self):
        raise ValueError("ACS does not implement trail limits")

    def local_update_trails(self, njob, pos):
        self.ph_trails[njob][pos] = np.add(np.multiply(np.subtract(1, self.xi),
                                                       self.ph_trails[njob][pos]),
                                           np.multiply(self.xi, self.tau0))

    def global_update_trails(self):
        """
        Update trails using the following rule:
        For all job i scheduled in position j, apply:

        trail[i][j] = (1 - rho) * trail[i][j] + rho * delta_trail

        where delta_trail is 1/T with T the total_wt of the current solution.
        """
        delta_trail = 1 / self.total_wt(self.solution)
        for pos, njob in enumerate(self.solution):
            self.ph_trails[njob][pos] = np.add(np.multiply(np.subtract(1, self.rho),
                                                           self.ph_trails[njob][pos]),
                                               np.multiply(self.rho, delta_trail))


class ACOMMAS(ACO):
    def __init__(self, **kwargs):
        # Limits are set in initialize_params(),
        # because it requires the initial heurisitic solution
        self.trail_max, self.trail_min = None, None
        self.limit_ratio = kwargs.pop('limit_ratio')
        super(ACOMMAS, self).__init__('mmas', **kwargs)

    def initialize_params(self):
        self.update_trail_limits()

    def initial_trail(self):
        if self.t > 1:
            logger.warning("Accessing initial trail after initialization! "
                           "(Ignore if reinit enabled)")
        return self.trail_max

    def update_trail_limits(self):
        """
        Update trail limits using the following formula:
        trail_max = 1/ rho T*
        where T* is the total weighted tardiness of the best-so-far solution.
        """
        self.trail_max = np.divide(1, np.multiply(self.rho, self.total_wt(self.solution)))
        self.trail_min = self.trail_max / self.limit_ratio

    def global_update_trails(self):
        """
        Update trails using the following rule:
        For all job i scheduled in position j, apply:

        trail[i][j] = (1 - rho) * trail[i][j] + delta_trail

        where delta_trail is 1/T with T the total_wt of the current solution.
        """
        delta_trail = 1 / self.total_wt(self.solution)
        for pos, njob in enumerate(self.solution):
            self.ph_trails[njob][pos] = np.add(np.multiply(np.subtract(1, self.rho),
                                                           self.ph_trails[njob][pos]),
                                               delta_trail)

    def local_update_trails(self, njob, pos):
        raise ValueError("MMAS does not implement local update")


def run_all(folder):
    errors = []
    for instance in glob.glob(f'{folder}/*'):
        try:
            PFSPWT(instance)
        except (RuntimeError, ValueError) as e:
            errors.append((instance, e))
    if errors:
        logger.error(f"Found errors in {len(errors)} instances")
        for instance, error in errors:
            print(f"{instance}: {error}", file=sys.stderr)


class FloatRange(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __eq__(self, other):
        return self.start <= other <= self.end

    def __repr__(self):
        return f"range {self.start} - {self.end}"


def parse_args(defaults, args):
    parser = argparse.ArgumentParser(description='Ant Colony Optimization for PFSP-WT')
    mutex = parser.add_mutually_exclusive_group(required=True)
    mutex.add_argument('--acs', action="store_true",
                       help='Apply Ant Colony System')
    mutex.add_argument('--mmas', action="store_true", help='Apply MAX-MIN Ant System')

    parser.add_argument('--instance', '-i', type=str, help='PFSP-WT instance file', required=True)
    parser.add_argument('--ants', '-m', type=int, default=defaults['ants'],
                        help=f"Number of ants to build every iteration, "
                        f"default={defaults['ants']}")
    parser.add_argument('--iterations', '-it', type=int, default=defaults['iterations'],
                        help=f"Maximum number of iterations to perform, "
                        f"default={defaults['iterations']}")
    parser.add_argument('--steps', '-st', type=int, default=defaults['steps'],
                        help=f"Maximum number of steps in each iterations "
                        f"(one step = one schedule built by an ant), "
                        f"default={defaults['steps']}")
    parser.add_argument('--max-time', '-mt', type=int, default=defaults['max-time'],
                        help=f"Maximum execution time (in seconds), "
                        f"default={defaults['max-time']}")
    parser.add_argument('--local-search', type=str, default=defaults['local-search'],
                        choices=['all', 'best', 'disable'],
                        help=f"Enable  O(n²) local search, 'best' enables local search "
                        f"only for the iteration-best ant, 'all' enables local search for all ants "
                        f", 'disable' disables local search"
                        f", default={defaults['local-search']}")
    parser.add_argument('--seed', '-s', type=int, default=defaults['seed'],
                        help=f"Number for the random seed generator. "
                        f"If negative the parameter is ignored and uses the current system time, "
                        f"default={defaults['seed']}")
    parser.add_argument('--alpha', '-a', type=float, default=defaults['alpha'],
                        help=f"Alpha parameter for relative influence of pheromone trail, "
                        f"default={defaults['alpha']}")
    parser.add_argument('--beta', '-b', type=float, default=defaults['beta'],
                        help=f"Beta parameter for relative influence of heuristic information, "
                        f"default={defaults['beta']}")
    parser.add_argument('--rho', '-r', type=float, choices=[FloatRange(0.001, 1.0)],
                        default=defaults['rho'],
                        help=f"Rho parameter for pheromone evaporation, "
                        f"default={defaults['rho']}")
    parser.add_argument('--xi', '-x', type=float, choices=[FloatRange(0.001, 1.0)],
                        default=defaults['xi'],
                        help=f"Xi parameter for local pheromone update (only for ACS), "
                        f"default={defaults['xi']}")
    parser.add_argument('--q0', '-q', type=float,
                        default=defaults['usebest'], choices=[FloatRange(0.0, 1.0)],
                        help=f"probability of best choice in tour construction (only for ACS), "
                        f"default={defaults['usebest']}")
    parser.add_argument('--heuristic', type=str, default=defaults['heuristic'],
                        choices=['edd', 'random'],
                        help=f"Method to compute the heuristic information, "
                        f"Earliest Due Dates (edd) or random initial solution (random)"
                        f"default={defaults['heuristic']}")
    parser.add_argument('-lr', '--limit-ratio', type=float, default=defaults['limit-ratio'],
                        help=f"Ratio for taumax - taumin: taumin = taumax / lr (only for MMAS), "
                        f" default={defaults['limit-ratio']}")
    parser.add_argument('-rta', '--reinit-trails-after', type=int,
                        default=defaults['reinit-trails-after'],
                        help=f"Iteration interval to check "
                        f"if there is stagnation and reinit. trails. Set to 0 to disable, "
                        f" default={defaults['reinit-trails-after']}")
    parser.add_argument('--log-level', '-l', type=str, default=defaults['log-level'],
                        choices=['debug', 'info', 'warning', 'error', 'notset'],
                        help=f"Logging level, "
                        f"default={defaults['log-level']}")
    parser.add_argument('--collect-data', action='store_true', default=defaults['collect-data'],
                        help=f"Collect data and store them in data folder, "
                        f"default={defaults['collect-data']}")
    parser.add_argument('--data-folder', type=str, default=defaults['data-folder'],
                        help=f"Path to folder where to store collected data, "
                        f"default={defaults['data-folder']}")
    return parser.parse_args(args)


def cmdline(args=None):
    """
    Execute acopfsp using command line arguments. If not arguments are given
    as parameters, sys.argv arguments are used.
    :param args: command line arguments, list format. Use None, use sys.argv as arguments.
    :return: (solution, total_wt, elapsed_time)
    """
    defaults = {
        'ants': 10,
        'iterations': 200,
        'steps': 100000,
        'max-time': 30,
        'local-search': 'disable',
        'seed': -1,
        'beta': 1,
        'alpha': 1,
        'rho': 0.1,
        'xi': 0.1,
        'usebest': 0.9,
        'log-level': 'notset',
        'heuristic': 'edd',
        'limit-ratio': 5,
        'reinit-trails-after': 100,
        'collect-data': False,
        'data-folder': './data/'
    }
    kwargs = vars(parse_args(defaults, args))
    apply_acs, apply_mmas = kwargs.pop('acs'), kwargs.pop('mmas')
    if apply_acs == apply_mmas:
        raise ValueError("Cannot apply ACS and MMAS at the same time")
    elif apply_acs:
        algo = ACOACS
        limit_ratio = kwargs.pop('limit_ratio')
        if limit_ratio != defaults['limit-ratio']:
            logger.warning("A limit ratio was provided with ACS, "
                           "but since it is not used by ACS, it will be ignored")
    elif apply_mmas:
        algo = ACOMMAS
    else:
        raise ValueError(f"Unknown algorithm, select acs or mmas")
    instance = kwargs.pop('instance')
    log_level = kwargs.pop('log_level')
    logger.setLevel(log_level.upper())
    kwargs['nants'] = kwargs.pop('ants')
    kwargs['usebest'] = kwargs.pop('q0')
    kwargs['localsearch'] = kwargs.pop('local_search')
    kwargs['is_acs'] = apply_acs
    kwargs['is_mmas'] = apply_mmas
    a = algo(pfsp_wt=PFSPWT(instance), **kwargs)
    try:
        return a.run()
    except KeyboardInterrupt:
        print("Interupted.", file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    cmdline()
