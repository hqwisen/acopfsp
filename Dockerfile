FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential time r-base \
                    r-cran-randomforest python3.6 python3-pip python3-setuptools python3-dev && \
    R -e "install.packages('irace', repos='http://cran.rstudio.com/')"

COPY requirements.txt ./requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt && \
    pip3 install --no-cache-dir ipython

RUN groupadd --gid 1000 aco && \
    useradd --create-home --gid 1000 --uid 1000 aco

ENV IRACEHOME="/usr/local/lib/R/site-library/irace"
ENV PATH="${IRACEHOME}/bin:${PATH}"

USER aco
WORKDIR /home/aco

COPY --chown=aco:aco *.py ./
COPY --chown=aco:aco tuning     ./tuning
COPY --chown=aco:aco instances  ./instances

CMD ["python3", "acopfsp.py"]